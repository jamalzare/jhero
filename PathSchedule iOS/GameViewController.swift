//
//  GameViewController.swift
//  Jhero iOS
//
//  Created by Jamal on 9/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit


class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let skView = self.view as! SKView
        
        maxApspectRatio = view!.frame.height/view!.frame.width
        var scene = GameScene(size: CGSize(width: 1242, height: 2688))
        if maxApspectRatio < 2{
            scene = GameScene(size: CGSize(width: 1536, height: 2048))
        }
    
        skView.presentScene(scene)
        
        skView.ignoresSiblingOrder = true
        skView.showsFPS = false
        skView.showsNodeCount = false
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}

//
//  TargetsManager.swift
//  Jhero
//
//  Created by Jamal on 12/31/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit



class TargetsManager{
    
    var scene: SKScene!
    var count = 3
    var remained = 0
    var score = 0
    
    init(scene: SKScene){
        self.scene = scene
    }
    
    private func checkIntersection(target: Target)-> Bool{
        var intersects = false
        scene.enumerateChildNodes(withName: "target"){ node, _ in
            if target.hash != node.hash, target.intersects(node){
                intersects = true
            }
        }
        return intersects
    }
    
    private func addTarget(){
        let target = Target(circleOfRadius: ballRadius)
        scene.addChild(target)
        if score>nextChalengeScore{
            target.duration = TimeInterval(randomCGFloat(0.8, 1.2))
        }
        target.animate()
        repeat {
            target.position = randomPoint()
        }while checkIntersection(target: target)
        
    }
    
    func addTargets(){
        for _ in 1...count{
            addTarget()
        }
    }
    
    func removeAllTargets(){
        scene.enumerateChildNodes(withName: "target"){ target, _ in
            target.removeFromParent()
        }
    }
    
    func showHiddensTargets(){
        scene.enumerateChildNodes(withName: "target"){ target, _ in
            target.isHidden = false
        }
    }
    
    func isTaregetsIntersectLine(_ line: Line)-> Bool{
        var c = 0
        scene.enumerateChildNodes(withName: "target"){target, _ in
            if target.intersects(line){
                c += 1
            }
        }
        return c == count
    }
    
    func addNextLevelTargets(score: Int){
        removeAllTargets()
        count = score + 1
        if score > nextChalengeScore{
            count -= nextChalengeScore
        }
        self.score = score
        remained = count
        addTargets()
    }
    
    func restartLevel(){
        remained = count
        showHiddensTargets()
    }
    
    func catched(target: SKNode){
        target.isHidden = true
        remained -= 1
        
        if soundIsOn{
            scene.run(targetSound)
        }
    }
    
}

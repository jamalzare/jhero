//
//  NodeFactory.swift
//  Jhero
//
//  Created by Jamal on 12/31/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

protocol NodeFactoryProtocol {
    var scene: SKScene? { get }
    var ball: Ball! {get set}
    var line: Line! {get set}
    
    func playButtonTapped()
}

class NodeFactory{
    
    var delegate: NodeFactoryProtocol!
    
    var header: Header!
    private var footer: Footer!
    private var menu: Menu!
    private var livesLabel: SKLabelNode!
    
    var scene: SKScene{
        return delegate.scene!
    }
    
    var isMenuShowning: Bool{
        return menu.isShowing
    }
    
    
    init(delegate: NodeFactoryProtocol) {
        self.delegate = delegate
    }
    
    func addMainShape(){
        //           mainShape = addShapeWith(rect: mainArea,
        //                                    strokeColor: .clear,
        //                                    lineWidth: 5)
    }
    
    func addMainLines(){
        
        
        let size = CGSize(width: mainArea.width+200, height: 10)
        var position = CGPoint(x: mainArea.midX, y: mainArea.maxY)
        
        let _ = scene.addShapeWith(size: size, position: position, name: "lineer", fillColor: themeColor)
        
        position.y = mainArea.minY
        let _ = scene.addShapeWith(size: size, position: position,name: "lineer2", fillColor: themeColor)
    }
    
    func addHeader(){
        let rect = CGRect(x: gameArea.minX, y: mainArea.maxY ,
                          width: gameArea.width, height: 200)
        header = Header(parentNode: scene, rect: rect)
    }
    
    func addMenu(){
        let size = CGSize(width: mainArea.width * 0.6,
                          height: mainArea.width * 0.7)
        let position = CGPoint(x: gameArea.midX, y: gameArea.midY)
        
        menu = Menu(delegate: self, size: size, position: position)
        menu.delegate = self
    }
    
    func addFooter(){
        let rect = CGRect(x: gameArea.minX, y: mainArea.minY - 300,
                          width: gameArea.width, height: 250)
        footer = Footer(parentNode: scene, rect: rect)
    }
    
    func addNodes(){
        addLabel()
        addMainLines()
        addHeader()
        addFooter()
        addMenu()
        addBall()
        addLine()
    }
    
    func addBall(){
        if delegate.ball != nil{
            delegate.ball.removeFromParent()
        }
        delegate.ball = Ball(circleOfRadius: ballRadius/2)
        scene.addChild(delegate.ball)
    }
    
    func addLine(){
        if delegate.line != nil{
            delegate.line.removeFromParent()
        }
        delegate.line = Line()
        scene.addChild(delegate.line)
    }
    
    func addLabel(){
        let color = Color.white.withAlphaComponent(0.1)
        livesLabel = scene.addLabelWith(px: mainArea.midX, py: mainArea.minY+20, color: color, fontSize: 80, name: "liveLabel", text: "")
    }
    
    func setScore(_ score: Int){
        header.score = score
    }
    
    func setLives(_ lives: Int){
        livesLabel.text = "x\(lives>0 ? lives: 0)"
        UserDefaults.standard.set(lives, forKey: "lives")
    }
    
    func showMenu(message: String = ""){
        menu.show(message: message)
    }
    
    func closeMenu(){
        menu.close()
    }
    
    func checkIfButtonTapped(point: CGPoint){
        menu.ifButtonTouched(point: point)
    }
    
    func changeColorFor(node: SKNode, color: Color){
        
        for child in node.children{
            changeColorFor(node: child, color: color)
        }
        
        let hash = themeColor.hash
        
        if let label = node as? SKLabelNode, label.fontColor!.hash == hash {
            label.fontColor = color
        }
        
        guard let shape = node as? SKShapeNode else{ return }
        
        if shape.fillColor.hash == hash {
            shape.fillColor = color
        }
        if shape.strokeColor.hash == hash {
            shape.strokeColor = color
        }
        
    }
    
    func changeThemeColorIfNeeded(color: Color){
        if themeColor.hash == color.hash{return}
        changeColorFor(node: scene, color: color)
        themeColor = color
    }
    
}


extension NodeFactory: MenuProtocol{
    
    func playButtonTapped() {
        delegate.playButtonTapped()
    }
    
    func presentHelp() {
        let help = HelpScene(size: scene.size)
        scene.view?.presentScene(help, transition: SKTransition.crossFade(withDuration: 0.3))
    }
}

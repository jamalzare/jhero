//
//  header.swift
//  Jhero
//
//  Created by Jamal on 12/1/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit
class Header{
    
    var score = 0{
        didSet{
            
            scoreLabel.text = "score: \(score>0 ? score: 0)"
            if score>0{
                scoreLabel.shake(duration: 1)
            }
            if score>record{
                record = score
            }
            UserDefaults.standard.set(score, forKey: "score")
        }
    }
    
    var record = 0{
        didSet{
            recordLabel.text = "record: \(record)"
            UserDefaults.standard.set(score, forKey: "record")
        }
    }
    
    var baseNode: SKShapeNode!
    var scoreLabel: SKLabelNode!
    var recordLabel: SKLabelNode!
    
    
    func createBase(parentNode: SKNode, rect: CGRect){
        baseNode = parentNode.addShapeWith(rect: rect, strokeColor: .clear, lineWidth: 5)
    }
    
    func addScoreLabel(){
        
        let frame = baseNode.frame
        scoreLabel = baseNode.addLabelWith(color: themeColor,
                                           name: "scoreLabel",
                                           alignment: .left,
                                           text: "score: 0")
        baseNode.addChild(scoreLabel)
        scoreLabel.position = CGPoint(x: frame.minX + 20, y: frame.midY)
    }
    
    func addRecordLabel(){
        let frame = baseNode.frame
        recordLabel = baseNode.addLabelWith(color: themeColor,
                                            name: "scoreLabel",
                                            alignment: .right,
                                            text: "record: 0")
        recordLabel.position = CGPoint(x: frame.maxX - 20, y: frame.midY )
        
        baseNode.addChild(recordLabel)
    }
    
    init(parentNode: SKNode, rect: CGRect){
        createBase(parentNode: parentNode, rect: rect)
        addScoreLabel()
        addRecordLabel()
        addMenu()
        
        let x = UserDefaults.standard.integer(forKey: "record")
        recordLabel.text = "record: \(x)"
        record = x
    }
    
    func addMenu(){
       let _ = baseNode.addShapeWith(circleOfRadius:60, pX: baseNode.frame.midX, pY: baseNode.frame.midY,
                                     name: "menu", fillColor: themeColor, strokeColor: .darkGray, lineWidth: 15)
    }
    
    
}

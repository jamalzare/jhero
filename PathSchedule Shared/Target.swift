//
//  Guard.swift
//  Jhero iOS
//
//  Created by Jamal on 9/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class Target: SKShapeNode {
    
    var color: Color!
    var duration: TimeInterval!

    var isObstacle = true {
        didSet{
            fillColor = isObstacle ? .black : color
        }
    }
    
    func change(){
        isObstacle = !isObstacle
    }
    
    func setPhysicsBody(){
        physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 100, height: 100))
        physicsBody?.affectedByGravity = false
        physicsBody?.categoryBitMask = ContactCategories.target
        physicsBody?.contactTestBitMask = ContactCategories.ball
        physicsBody?.collisionBitMask = 0
        physicsBody?.usesPreciseCollisionDetection = true
    }
    
    func animate(){
        run(SKAction.repeatForever( SKAction.sequence([
            SKAction.scale(to: 1, duration: duration),
            SKAction.scale(to: 0, duration: duration),
            SKAction.run(change)
            ])))
        
    }
    
    override init(){
        super.init()
        name = "target"
        setPhysicsBody()
        strokeColor = .clear
        color = getRandomColor()
        duration = 1
    }
    
    
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


//
//  HelpScene.swift
//  Jhero iOS
//
//  Created by Jamal on 12/4/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class HelpScene: SKScene{
    
    var imageCount = 3
    var lastImageIndex = 0
    var gameArea: CGRect!
    var mainArea: CGRect!
    var mainShape: SKShapeNode!
    var image: SKSpriteNode!
    var imageIndexLabel: SKLabelNode!
    var descriptionLabel: SKLabelNode!
    
    func addMainShape(){
        mainShape = addShapeWith(rect: mainArea,
                                 strokeColor: .clear,
                                 lineWidth: 5)
    }
    
    func addMainLines(){
        
        let frame = mainShape.frame
        let size = CGSize(width: frame.width+200, height: 10)
        var position = CGPoint(x: frame.midX, y: frame.maxY)
        
        let _ = mainShape.addShapeWith(size: size, position: position, fillColor: themeColor)
        
        position.y = frame.minY
        let _ = mainShape.addShapeWith(size: size, position: position, fillColor: themeColor)
    }
    
    func addLabel(){
        let _ = addLabelWith(px: mainArea.midX, py: mainArea.maxY + 50, color: themeColor, text: "HELP")
    }
    
    func addImageIndexLabel(){
        imageIndexLabel = addLabelWith(px: mainArea.midX, py: mainArea.minY - 100, color: themeColor, text: "1 / 3")
    }
    
    func addDescriptionLabel(){
        
        descriptionLabel = addLabelWith(px: mainArea.midX,
                                        py: mainArea.minY + mainArea.height/2 + 50,
                                        color: themeColor,
                                        fontSize: 60,
                                        text: helpDescriptions[0])
        descriptionLabel.preferredMaxLayoutWidth = mainArea.width * 0.9
        descriptionLabel.horizontalAlignmentMode = .center
        descriptionLabel.verticalAlignmentMode = .top
        descriptionLabel.numberOfLines = 0
        
    }
    
    func setImage(){
    
        if let image = image{
            image.removeFromParent()
        }
        let name = "helpImage\(lastImageIndex).png"
    
        image = addNodeWith(imageNamed: name,
                            width: mainArea.size.width, height: mainArea.size.height/2,
                            pX: mainArea.midX, pY: mainArea.midY + mainArea.size.height/4)
    }
    
    func nextImage(){
        
        lastImageIndex += 1
        if lastImageIndex > imageCount {
            view?.presentScene(baseScene)
            return
        }
        
        setImage()
        descriptionLabel.text = helpDescriptions[lastImageIndex - 1]
        imageIndexLabel.text = "\(lastImageIndex) / \(imageCount)"
    }
    
    override func didMove(to view: SKView) {
        addMainShape()
        addMainLines()
        addLabel()
        addImageIndexLabel()
        addDescriptionLabel()
        nextImage()
        backgroundColor = .appBlack
    }
    
    override init(size: CGSize) {
        
        let playAbleWidth = size.height / maxApspectRatio
        let margin = (size.width - playAbleWidth)/2
        gameArea = CGRect(x: margin, y: 0, width: playAbleWidth, height: size.height)
        mainArea = CGRect(x: gameArea.minX,
                          y: gameArea.midY - gameArea.width/2,
                          width: gameArea.width,
                          height: gameArea.width)
        
        super.init(size: size)
        self.scene?.scaleMode = .aspectFill
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


#if os(iOS)
extension HelpScene{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        nextImage()
    }
    
}
#endif

#if os(OSX)
extension HelpScene{
    
    override func mouseDown(with event: NSEvent) {
        nextImage()
    }
    
}
#endif

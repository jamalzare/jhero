//
//  Menu.swift
//  Jhero
//
//  Created by Jamal on 12/2/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

protocol MenuProtocol {
    var scene: SKScene {get}
    func playButtonTapped()
    func presentHelp()
}

class Menu{
    
    private var y = 110
    var delegate: MenuProtocol!
    var baseNode: SKShapeNode!
    var soundOn: SKShapeNode!
    private var messageLabel: SKLabelNode!
    
    var isShowing = true
    
    var scene: SKScene{
        return delegate.scene
    }
    
    var soundIsOn = true{
        didSet{
            soundOn.fillColor = soundIsOn ? themeColor: .darkGray
            if soundIsOn{
                baseNode.run(SKAction.playSoundFileNamed("colid.mp3", waitForCompletion: false))
            }
            //PathSchedule.soundIsOn = soundIsOn
            
            turnSound(on: soundIsOn)
        }
    }
    
    var message: String = "" {
        didSet{
            messageLabel.text = message
        }
    }
    
    init(delegate: MenuProtocol, size: CGSize, position: CGPoint) {
        self.delegate = delegate
        baseNode = scene.addShapeWith(size: size, cornerRadius: 20,
                                           position: position, fillColor: .appSecoundBlack)
        baseNode.zPosition = 11
        create()
    }
    
    
    func create(){
        
        createMessageLabel()
        createLabel(text: "Play")
        createLabel(text: "Sound")
        createLabel(text: "Help")
        soundOn = baseNode.addShapeWith(circleOfRadius: 30,
                                        pX: 200, pY: -40,
                                        name: "soundOn",
                                        fillColor: themeColor)
        
        
        
    }
    
    func createMessageLabel(){
        
        messageLabel = baseNode.addLabelWith(color: .green,
                                             name: "message",
                                             alignment: .center,
                                             text: "Start")
        messageLabel.position = CGPoint(x: 0, y: 300)
        baseNode.addChild(messageLabel)
    }
    
    func createLabel(text: String){
        
        let label = baseNode.addLabelWith(color: themeColor,
                                          name: text,
                                          alignment: .center,
                                          text: text)
        label.position = CGPoint(x: 0, y: y)
        y -= 175
        baseNode.addChild(label)
    }
    
    func show(message: String = "Start"){
        isShowing = true
        self.message = message
        baseNode.run(SKAction.scale(to: 1, duration: 0.3))
    }
    
    func close(){
        baseNode.run(SKAction.scale(to: 0, duration: 0.3)){
            self.isShowing = false
        }
        
    }
    
    func ifButtonTouched(point: CGPoint){
        let nodes = scene.nodes(at: point)
        guard !nodes.isEmpty, let name = scene.nodes(at: point)[0].name else{
            return
        }
        switch name {
        case "menu":
            show()
            
        case "Play":
            delegate.playButtonTapped()
            close()
            
        case "Help":
            delegate.presentHelp()
            close()
            
        case "Sound":
            soundIsOn = !soundIsOn
        default:
            return
        }
    }
}

//
//  Ball.swift
//  Jhero
//
//  Created by Jamal on 9/19/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class Ball: SKShapeNode{
    
    var label: SKLabelNode!
    var isMoving = false
    
    func setPhysicsBody(){
        physicsBody = SKPhysicsBody(circleOfRadius: 25)
        physicsBody?.affectedByGravity = false
        physicsBody?.categoryBitMask = ContactCategories.ball
        physicsBody?.contactTestBitMask = ContactCategories.obstacle | ContactCategories.target
        physicsBody?.collisionBitMask = 0
        physicsBody?.isDynamic = true
    }
    
    
    override init() {
        super.init()
        name = "ball"
        setPhysicsBody()
        fillColor = themeColor
        strokeColor = .clear
        position = CGPoint(x: 7000, y: 1000)
        zPosition = 1
        
    }
    
    func move(path: CGPath, callBack: @escaping ()->Void){
        if isMoving || path.isEmpty {return}
        
        isMoving = true
        
        run(SKAction.sequence([
            SKAction.follow(path, asOffset: false, orientToPath: true, speed: 400),
            SKAction.run {
                callBack()
                self.isMoving = false
            }
        ]))
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

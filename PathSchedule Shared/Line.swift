//
//  Line.swift
//  Jhero iOS
//
//  Created by Jamal on 9/24/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class Line: SKShapeNode{
    
//    var startToDraw = true
    var drawPath: CGMutablePath!
    
    func draw(location: CGPoint, previos: CGPoint){
        
        if drawPath == nil{
            drawPath = CGMutablePath()
            drawPath.move(to: previos)
        }
        drawPath.addLine(to: location)
        path = drawPath
        //startToDraw = false
        
    }
    
    func clear(){
        removeFromParent()
    }
    
    
    override init() {
        super.init()
        lineWidth = 10
        strokeColor = themeColor
        lineCap = .round
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  Global.swift
//  Jhero
//
//  Created by Jamal on 12/31/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit


// Global Members
var textFontSize: CGFloat = 70
var appFontName: String = "ChalkboardSE-Bold"
var themeColor: Color = .appOrange

var maxApspectRatio: CGFloat = 1
var gameArea: CGRect = CGRect.zero
var mainArea: CGRect = CGRect.zero
let ballRadius: CGFloat = 40
var baseScene: SKScene!

let headHeight: CGFloat = 300
let footerHeight: CGFloat = 400

let nextChalengeScore = 9

var soundIsOn = true
let targetSound = SKAction.playSoundFileNamed("colid.mp3", waitForCompletion: false)

//AppStoreConnect INfo
let supportURL = "https://jpathschedule.github.io"
let privacyPolicyURL = "https://jpathschedule.github.io/privacypolicy"
let EmailForPrivacyAndSupport = "JPathSchedule@gmail.com"
let copyRight = "2020 Jafar Abedi, All rights reserved."
let version = "1.0"

let s = "yeah"

func turnSound(on: Bool){
    soundIsOn = on
}


enum GameState{
    case none
    case started
    case ended
}

struct ContactCategories {
    static let none: UInt32 = 0
    static let ball: UInt32 = 0b1 //1
    static let obstacle: UInt32 = 0b10//2
    static let target: UInt32 = 0b100//4
}

func randomPoint() -> CGPoint{
    return CGPoint(x: randomXPoint(), y: randomYPoint())
}

func randomXPoint() -> CGFloat{
    return  randomCGFloat(mainArea.minX + ballRadius, mainArea.maxX - ballRadius)
}

func randomYPoint() -> CGFloat{
    return  randomCGFloat(mainArea.minY + ballRadius ,mainArea.maxY - ballRadius)
}

func getRandomColor()-> Color{
    return Color.getRandomMDColor()
}


var helpDescriptions =  [
"swipe to draw a PATH that intersects all colored circle shape targets, the orange BALL moves on the path that you drew",

 "when the ball reaches the colored circle targets, balls durability is increased and when it reaches the black circle, durability is reduced",

"if the ball reach all colored targets your score will increase and you will go to the next level, if the ball reaches any black circle the level will restart, when durability reduced to 0, game is over",
]

//
//  GameScene.swift
//  Jhero Shared
//
//  Created by Jamal on 9/9/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import SpriteKit
import GameplayKit

/*
 tasks:

 1-     test in devices
 3-     sound loading hanges first time
 
 
 **/


class GameScene: SKScene {
    
    var line: Line!
    var ball: Ball!
    var factory: NodeFactory!
    var targetsManger: TargetsManager!
    var gameLoaded = false
    var onceLevelLoaded = false
    
    
    var lives = 3 {
        didSet {
            factory.setLives(lives)
        }
    }
    
    var score = 0 {
        didSet {
            factory.setScore(score)
        }
    }
    
    var canDrawLine: Bool{
        return !(ball.isMoving || factory.isMenuShowning)
    }
    
    var canMoveBall: Bool{
        return !(factory.isMenuShowning || line.drawPath == nil)
        
    }
    
    var isLineMovable: Bool{
        return line == nil ? false: targetsManger.isTaregetsIntersectLine(line)
    }
    
    func add(px: CGFloat, py: CGFloat, color: Color){
        let _ = addShapeWith(circleOfRadius: ballRadius, pX: px, pY: py, fillColor: color)
    }
    
    func addIconTarget(){
        add(px: 1000, py: 2000, color: .redMD)
        add(px: 700, py: 1800, color: .black)
        add(px: 600, py: 1600, color: .lightBlueMD)
        add(px: 900, py: 1700, color: .greenMD)
    }
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        
        if !gameLoaded{
            factory = NodeFactory(delegate: self)
            factory.addNodes()
            targetsManger = TargetsManager(scene: self)
            backgroundColor = .appBlack
            gameLoaded = true
            setLivesAndScoreFromMemory()
            
            //addIconTarget()
            
        }
    }
    
    func setLivesAndScoreFromMemory(){
        let lives = UserDefaults.standard.integer(forKey: "lives")
        if lives>0{
            self.lives = lives
            score = UserDefaults.standard.integer(forKey: "score")
        }
        
    }
    
    func drawLine(location: CGPoint, previos: CGPoint){
        if mainArea.contains(location) && canDrawLine {
            line.draw(location: location, previos: previos)
        }
    }
    
    func checkLine(){
        isLineMovable ? moveBall(): factory.addLine()
    }
    
    func moveBall(){
        if canMoveBall{
            ball.move(path: line.drawPath, callBack: {
                self.afterMove()
            })
        }
    }
    
    func afterMove(){
        factory.addLine()
        factory.addBall()
        if targetsManger.remained == 0 {
            score += 1
            nextLevel()
        }else{
            restartLevel()
        }
        
    }
    
    
    func nextLevel(){
        
        if score == 0 {
            lives = 3
        }
        
        if score > nextChalengeScore{
            factory.changeThemeColorIfNeeded(color: .limeMD)
        }
        targetsManger.addNextLevelTargets(score: score)
    }
    
    func restartLevel(){
        targetsManger.restartLevel()
    }
    
    func gameOver(){
        addExplosion(position: ball.position)
        factory.showMenu(message: "Game Over")
        score = 0
        lives = 0
        
        factory.addBall()
        factory.addLine()
        factory.changeThemeColorIfNeeded(color: .appOrange)
    }
    
    func touchNode(location: CGPoint){
        factory.checkIfButtonTapped(point: location)
    }
    
    override init(size: CGSize) {
        
        let playAbleWidth = size.height / maxApspectRatio
        let margin = (size.width - playAbleWidth)/2
        gameArea = CGRect(x: margin, y: 0, width: playAbleWidth, height: size.height)
        mainArea = CGRect(x: gameArea.minX,
                          y: gameArea.minY + footerHeight,
                          width: gameArea.width,
                          height: gameArea.height - (headHeight + footerHeight))
        
        super.init(size: size)
        scene?.scaleMode = .aspectFill
        baseScene = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//MENU PROTOCOLS INSIDE FACTORY
extension GameScene: NodeFactoryProtocol{
    
    func playButtonTapped(){
        
        if !onceLevelLoaded{
            nextLevel()
            onceLevelLoaded = true
            run(targetSound)
            return
        }
        
        if lives == 0{
            score = 0
            nextLevel()
        }
    }
}

extension GameScene: SKPhysicsContactDelegate{
    
    func changeBallColor(color: Color){
        ball.fillColor = color
        run(SKAction.wait(forDuration: 0.4)){
            self.ball.fillColor = themeColor
        }
    }
    
    func obstacleCollison() {
        
        lives -= 2
        
        if lives < 1{
            gameOver()
            return
        }
        
    }
    
    func targetCollison(_ target: SKNode){
        lives += 1
        addExplosion(position: target.position)
        targetsManger.catched(target: target)
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        let node = (contact.bodyA.node is Target) ?
            contact.bodyA.node: contact.bodyB.node
        
        if let node = node, node.isHidden == false{
            let target = (node as! Target)
            changeBallColor(color: target.fillColor)
            target.isObstacle ? obstacleCollison(): targetCollison(target)
        }
        
    }
    
    func addExplosion(position: CGPoint){
        
        let explosion = SKEmitterNode(fileNamed: "Explode")
        explosion?.position = position
        addChild(explosion!)
        
        
        run(SKAction.wait(forDuration: 2)){
            explosion?.removeFromParent()
        }
    }
    
}


#if os(iOS)
extension GameScene{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            touchNode(location: touch.location(in: self))
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches{
            let location = touch.location(in: self)
            let previos = touch.previousLocation(in: self)
            drawLine(location: location, previos: previos)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        checkLine()
    }
    
}
#endif

#if os(OSX)
extension GameScene{
    
    override func mouseDragged(with event: NSEvent) {
        drawLine(location: event.location(in: self), previos: event.location(in: self))
    }
    
    override func mouseUp(with event: NSEvent) {
        checkLine()
    }
    
    override func mouseDown(with event: NSEvent) {
        touchNode(location: event.location(in: self))
    }
    
}
#endif

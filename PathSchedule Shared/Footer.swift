//
//  Footer.swift
//  Jhero
//
//  Created by Jamal on 12/24/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

class Footer{
    
    var baseNode: SKShapeNode!
    var label: SKLabelNode!
    
    
    func createBase(parentNode: SKNode, rect: CGRect){
        baseNode = parentNode.addShapeWith(rect: rect, strokeColor: .clear, lineWidth: 5)
    }
    
    func addLable(){
        
        let frame = baseNode.frame
        label = baseNode.addLabelWith(color: themeColor,
                                      name: "footerLabel",
                                      text: "Welcom to JPath Schedule")
        baseNode.addChild(label)
        label.position = CGPoint(x: frame.midX, y: frame.midY)
    }
    
    
    
    init(parentNode: SKNode, rect: CGRect){
        createBase(parentNode: parentNode, rect: rect)
        addLable()
        
    }
    
}


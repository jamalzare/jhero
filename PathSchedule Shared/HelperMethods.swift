//
//  HelperMethods.swift
//  JVO
//
//  Created by Jamal on 6/26/19.
//  Copyright © 2019 Jamal. All rights reserved.
//

import Foundation
import SpriteKit

#if os(OSX)
typealias Color = NSColor
typealias Image = NSImage
#else
typealias Color = UIColor
typealias Image = UIImage
#endif


func randomCGFloat(_ lowerLimit: CGFloat, _ upperLimit: CGFloat)-> CGFloat{
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upperLimit - lowerLimit) + lowerLimit
}

func randomCGFloat(_ upperLimit: CGFloat)-> CGFloat{
    return CGFloat(Float(arc4random()) / Float(UINT32_MAX)) * (upperLimit)
}

func randomInt(_ upperLimit: Int)-> Int{
    return Int(randomCGFloat(CGFloat(upperLimit)))
}

func randomInt(_ lowerLimit: Int, _ upperLimit: Int)-> Int{
    return Int(randomCGFloat(CGFloat(lowerLimit), CGFloat(upperLimit)))
}

extension Color{
    
    //MD: Material Design
    
    class var appOrange: Color {
        //return Color(hexString: "#F57C00")
        //return Color(red: 252/255, green: 45/255, blue: 30/255, alpha: 1)
        return rgba(255, 115, 100)
    }
    
    class var appBlack: Color{
        //return Color.darkGray
        //return Color(red: 62/255, green: 63/255, blue: 58/255, alpha: 1)
        return rgba(41, 42, 48)
    }
    
    class var appSecoundBlack: Color{
        return rgba(47, 50, 57)
    }
    
    class var redMD: Color {
        return Color(hexString: "#F44336")
    }
    
    class var pinkMD: Color {
        return Color(hexString: "#E91E63")
    }
    
    class var purpleMD: Color {
        return Color(hexString: "#9C27B0")
    }
    
    class var deepPurpleMD: Color {
        return Color(hexString: "#673AB7")
    }
    
    class var indigoMD: Color {
        return Color(hexString: "#3F51B5")
    }
    
    class var blueMD: Color {
        return Color(hexString: "#3F51B5")
    }
    
    class var lightBlueMD: Color {
        return Color(hexString: "#03A9F4")
    }
    
    class var cyanMD: Color {
        return Color(hexString: "#00BCD4")
    }
    
    class var tealMD: Color {
        return Color(hexString: "#009688")
    }
    
    class var greenMD: Color {
        return Color(hexString: "#4CAF50")
    }
    
    class var lightGreenMD: Color {
        return Color(hexString: "#8BC34A")
    }
    
    class var limeMD: Color {
        return Color(hexString: "#CDDC39")
    }
    
    class var yellowMD: Color {
        return Color(hexString: "#FFEB3B")
    }
    
    class var amberMD: Color {
        return Color(hexString: "#FFC107")
    }
    
    class var orangeMD: Color {
        return Color(hexString: "#FF9800")
    }
    
    class var deepOrangeMD: Color {
        return Color(hexString: "#FF5722")
    }
    
    class var brownMD: Color {
        return Color(hexString: "#795548")
    }
    
    class var grayMD: Color {
        return Color(hexString: "#9E9E9E")
    }
    
    class var blueGrayMD: Color {
        return Color(hexString: "#607D8B")
    }
    
    static var colors: [Color]{
        return[
            redMD, pinkMD, purpleMD, deepPurpleMD, indigoMD,
            blueMD, lightBlueMD, cyanMD, tealMD, blueGrayMD,
            greenMD, lightGreenMD, limeMD, yellowMD,
            amberMD, orangeMD, deepOrangeMD, brownMD, grayMD,
        ]
    }
    
    static func getRandomMDColor()-> Color{
        let x = randomInt(colors.count - 1)
        return colors[x]
    }
    
    public convenience init(hexString: String) {
        var cString:String = hexString.uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            print("not hex String")
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
        
    }
    
    class func rgba(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat = 1)-> Color{
        return Color(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
}

extension SKNode{
    
    func addNodeWith(fileName: String? = nil,
                     imageNamed: String? = nil,
                     texture: SKTexture? = nil,
                     width: CGFloat? = nil,
                     height: CGFloat? = nil,
                     size: CGSize? = nil,
                     position: CGPoint? = nil,
                     pX: CGFloat? = nil,
                     pY: CGFloat? = nil,
                     name: String? = nil,
                     color: Color? = nil
    )-> SKSpriteNode{
        
        var node: SKSpriteNode!
        var size = size
        
        if let width = width, let height = height {
            size = CGSize(width: width, height: height)
        }
        
        if let imageNamed = imageNamed{
            node = SKSpriteNode(imageNamed: imageNamed)
            node.size = size!
        }
        
        
        if let texture = texture, let color = color, let size = size{
            node = SKSpriteNode(texture: texture, color: color, size: size)
        }
        
        if node == nil, let color = color, let size = size{
            node = SKSpriteNode(color: color, size: size)
        }
        
        if node == nil, let texture = texture, let size = size{
            node = SKSpriteNode(texture: texture, size: size)
        }
        
        if node == nil, let texture = texture{
            node = SKSpriteNode(texture: texture)
        }
        
        addChild(node)
        
        if let position = position{
            node.position = position
        }
        
        if let px = pX, let py = pY{
            node.position = CGPoint(x: px, y: py)
        }
        
        if let name = name{
            node.name = name
        }
        
        return node
    }
    
    
    func addShapeWith(x: CGFloat? = nil,
                      y: CGFloat? = nil,
                      width: CGFloat? = nil,
                      height: CGFloat? = nil,
                      rect: CGRect? = nil,
                      size: CGSize? = nil,
                      cornerRadius: CGFloat = 0,
                      circleOfRadius: CGFloat? = nil,
                      position: CGPoint? = nil,
                      pX: CGFloat? = nil,
                      pY: CGFloat? = nil,
                      name: String? = nil,
                      fillColor: Color = Color.clear,
                      strokeColor: Color = Color.clear,
                      lineWidth: CGFloat? = nil
    )-> SKShapeNode{
        
        var shape: SKShapeNode!
        
        if let rect = rect{
            shape = SKShapeNode(rect: rect, cornerRadius: cornerRadius)
        }
        
        if let size = size{
            shape = SKShapeNode(rectOf: size, cornerRadius: cornerRadius)
        }
        
        if let width = width, let height = height {
            let size = CGSize(width: width, height: height)
            shape = SKShapeNode(rectOf: size, cornerRadius: cornerRadius)
        }
        
        if let x = x , let y = y, let width = width, let height = height {
            let rect = CGRect(x: x, y: y, width: width, height: height)
            shape = SKShapeNode(rect: rect, cornerRadius: cornerRadius)
        }
        
        if let radius = circleOfRadius{
            shape = SKShapeNode(circleOfRadius: radius)
        }
        
        addChild(shape)
        
        if let position = position{
            shape.position = position
        }
        
        if let px = pX, let py = pY{
            shape.position = CGPoint(x: px, y: py)
        }
        
        if let name = name{
            shape.name = name
        }
        
        if let width = lineWidth{
            shape.lineWidth = width
        }
        
        shape.fillColor = fillColor
        shape.strokeColor = strokeColor
        
        return shape
    }
    
    func addLabelWith(color: Color = .orange,
                      fontSize size: CGFloat = textFontSize,
                      fontName: String = appFontName,
                      name: String = "",
                      alignment: SKLabelHorizontalAlignmentMode = .center,
                      text: String = "")-> SKLabelNode{
        let label = SKLabelNode()
        label.fontColor = color
        label.fontSize = size
        label.fontName = fontName
        label.name = name
        label.horizontalAlignmentMode = alignment
        label.text = text
        return label
    }
    
    func addLabelWith(position: CGPoint? = nil,
                      px: CGFloat? = nil,
                      py: CGFloat? = nil,
                      color: Color = .orange,
                      fontSize size: CGFloat = textFontSize,
                      fontName: String = appFontName,
                      name: String = "",
                      alignment: SKLabelHorizontalAlignmentMode = .center,
                      text: String = "")-> SKLabelNode{
        let label = SKLabelNode()
        label.fontColor = color
        label.fontSize = size
        label.fontName = fontName
        label.name = name
        label.horizontalAlignmentMode = alignment
        label.text = text
        addChild(label)
        
        if let position = position{
            label.position = position
        }
        
        if let px = px, let py = py{
            label.position = CGPoint(x: px, y: py)
        }
        
        return label
    }
    
    func playSound(file: String){
        run(SKAction.playSoundFileNamed(file, waitForCompletion: false))
    }
    
    func shake(duration:Float) {
        
        let numberOfShakes = duration / 0.4
        var actionsArray:[SKAction] = []
        
        for _ in 1...Int(numberOfShakes) {
            
            let shakeAction = SKAction.moveBy(x: 100, y: 0, duration: 0.2)
            shakeAction.timingMode = SKActionTimingMode.easeOut
            actionsArray.append(shakeAction)
            actionsArray.append(shakeAction.reversed())
        }
        
        let actionSeq = SKAction.sequence(actionsArray);
        self.run(actionSeq);
    }
    
}
